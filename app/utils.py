from typing import Any

import json


def load_candidates(filename: str) -> list:
    with open(filename, encoding='UTF-8') as file:
        data = json.load(file)
    return data

def get_all() -> list:
    return load_candidates('candidates.json')


def get_by_pk(pk: int) ->list:
    data = get_all()
    return [i for i in data if i['pk']==pk]


def get_by_skill(skill_name: str) ->list:
    data = get_all()

    return [i for i in data 
            if skill_name.lower() in i['skills'].lower().split(', ')
            ]


