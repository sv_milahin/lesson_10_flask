from flask import Flask, render_template


from utils import get_all, get_by_pk, get_by_skill

app = Flask(__name__)


@app.route('/')
def home_view():
    data_all = get_all() 
    return render_template('index.html', data=data_all)
   

@app.route('/candidates/<int:pk>/')
def condidates_view(pk):
    data = get_by_pk(pk)
    return render_template('candidates.html', candidates=data)


@app.route('/skills/<skill_name>/')
def skills_view(skill_name):
    skills = get_by_skill(skill_name)
    return render_template('skills.html', skills=skills)



if __name__ == '__main__':
    app.run(debug=True)
